import React from 'react'

const Footer = () => {
  return (
    <div className='container flex justify-center'>
        <div className='flex gap-3 text-white'>
            <p>&copy;  2024 Gadget Hub</p>
            <p>|</p>
            <p>All rights reserved.</p>
        </div>
    </div>
  )
}

export default Footer